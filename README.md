# chammy

[![pipeline status](https://gitlab.com/chammyjs/chammy/badges/master/pipeline.svg)](https://gitlab.com/chammyjs/chammy/commits/master)
[![coverage report](https://gitlab.com/chammyjs/chammy/badges/master/coverage.svg)](https://gitlab.com/chammyjs/chammy/commits/master)

JavaScript project structure generator.
